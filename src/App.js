import React from 'react';
import './App.css';
import VoteComponent from './containers/vote';

function App() {
  return (
    <div className="App">
     <VoteComponent />
    </div>
  );
}

export default App;
