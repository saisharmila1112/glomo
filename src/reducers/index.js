import {combineReducers} from "redux"
import sportEvents from "../data/test-assignment.json"

const dataReducer = () => {
    return sportEvents
}

const eventsReducer = (state=null, action) =>  {
    switch(action.type){
        case "REG_ACTIVE_EVENTS" : return action.payload
        default : return state
    }
}

export default combineReducers({
    events : dataReducer,
    activeEvents : eventsReducer
})