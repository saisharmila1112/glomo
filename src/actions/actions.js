export const registerActiveEvents = (event1, event2) => {
    
    return {
        type : "REG_ACTIVE_EVENTS",
        payload : {
            event1, event2
        }
    }
}