import React, {useState} from "react"

const InputComponent = props => {

    const [checked, toggleChecked] = useState("option1")

    return <div className="radio__input_container">
        <div className="radio">
            <input onChange={()=> toggleChecked("option1")} checked={checked == "option1"} id="option1" type="radio" value="1"></input>
            <label htmlFor="option1">Home Team win (Team A)</label>
        </div>
        <div className="radio">
            <input onChange={()=> toggleChecked("option2")} checked={checked == "option2"} id="option2" type="radio" value="2"></input>
            <label htmlFor="option2">Draw</label>
        </div>
        <div className="radio">
            <input onChange={()=> toggleChecked("option3")} checked={checked == "option3"} id="option3" type="radio" value="3"></input>
            <label htmlFor="option3">Away Team win (Team B)</label>
        </div>
    </div>
}

export default InputComponent