import React from "react"

const EventComponent = props => {
    switch(props.type){
        case "homeTeam" : return <div className="event__name">{props.data.homeName}</div>
         
        case "awayTeam" : return <div className="event__name">{props.data.awayName}</div>
    }
    
}

export default EventComponent