import React from "react"
import {connect} from "react-redux"
import _ from "lodash"

import { bindActionCreators } from "redux";

import {registerActiveEvents} from "../actions/actions"
import EventComponent from "../components/eventComponent";
import InputComponent from "../components/inputComponent";


class VoteComponent extends React.Component{

    componentDidMount(){
        this.eventDecider()
    }

    shuffler(a){
        var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
    }

    eventDecider(){

        var counts = {}
        
         this.props.events.forEach(event => {
            counts[event.sport] = counts[event.sport] ? counts[event.sport] + 1 : 1 //to check if it exists atleast twice so that it can get an opponent
         }); 
       
        var toBeFiltered = Object.keys (_.pickBy(counts, function(value, key){ //this returns the sport names that occur less than 1 time
            return value <= 1
        }))
       
        var eligibleSports = []
        this.props.events.map(event => {
            if(!toBeFiltered.includes(event.sport)){
               eligibleSports.push(event)
            }
       })
       
       const uniqueSports = [...new Set(eligibleSports)]
       const activeSport =  uniqueSports[Math.floor(Math.random()*uniqueSports.length)].sport
       
       const activeEventSet = _.pickBy(uniqueSports, function(value, key){
            return value.sport == activeSport
       })
       const shuffledEventSet = this.shuffler(Object.keys(activeEventSet))
       const activeEvent1 = activeEventSet[`${shuffledEventSet[0]}`]
       const activeEvent2 = activeEventSet[`${shuffledEventSet[1]}`]

       this.props.registerActiveEvents(activeEvent1, activeEvent2)

    }

    render(){
        if(this.props.activeEvents === null){
            return <div>loading...</div>
        }else {
            const {event1, event2} = this.props.activeEvents
            return(
            <div className="main__container">
                <div className="event__container">
                    <EventComponent type="homeTeam" data={event1} />
                    <EventComponent type="awayTeam" data={event2} />
                </div>
                <div className="event__inputs">
                    <InputComponent />
                    <button className="event__btn" onClick={() => this.eventDecider()} >Next</button>
                </div>
            </div>
        )}
        
    }
}

function mapStateToProps(state){
    return  {
        events : state.events,
        activeEvents : state.activeEvents
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({registerActiveEvents}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(VoteComponent)